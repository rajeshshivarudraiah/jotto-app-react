import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers';

export const middleWare = [ReduxThunk];
const createStoreWithMiddleware = applyMiddleware(...middleWare)(createStore)

export default createStoreWithMiddleware(rootReducer);