import PropTypes from 'prop-types'
/**
 * Functional react component for congratulatory message
 * @function
 * @param {object} props - React props
 * @return {JSX.element} - Rendered component (or null if  `success` props is )
 */
// export default (props) =>{
//   return ( 
//       if(props.success){
//         <div data>
//         </div>
//       }
//   )
// }

import React from 'react'

function Congrats(props) {
    if(props.success){
      return (
        <div data-test="component-congrats" className="alert alert-success">
          <span data-test="congrats-message">
            Congratulations! You guessed the word!
          </span>
        </div>
      )
    }
    else{
      return(
        <div data-test="component-congrats" />
      )
    }
  
}

Congrats.propTypes ={
  success: PropTypes.bool.isRequired
}

export default Congrats;