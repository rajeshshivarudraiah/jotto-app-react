import checkPropTypes from 'check-prop-types'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../src/reducers'
import { middleWare} from '../src/configureStore'
/**
 * Creating a testing store with imported reducers, middleware, and intital state
 * globals: rootReducer
 * @param {Object} initialState - Inital state for store
 * @function storeFactory 
 * @returns {Store} - Redux store
 */
export const storeFactory = (initialState) =>{
  const createStoreWithMiddleWare = applyMiddleware(...middleWare)(createStore);

  return createStoreWithMiddleWare(rootReducer, initialState);
}
/**
 * Return node which gives data-attribute
 * @param {ShallowWrapper} wrapper - Enzyme shallow Wrapper
 * @param {string} val - Value of data-test attribute for search
 * @returns {ShallowWrapper}
 */

export const findByTestAttr = (wrapper,val) =>{
  return wrapper.find(`[data-test="${val}"]`)
}

export const Checkprops = (component, conformingProps) =>{
  const propError = checkPropTypes(component.propTypes, conformingProps, 'prop',component.name);
  expect(propError).toBeUndefined()

}