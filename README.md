-> Congrats Component
a) Child of App Component
b) App is connected component
   - Access to success piece of state
c) Pass success to Congrats as props

-> npm install prop-types
-> npm install --save-dev check-prop-types

Summary of Abstractions
-> findByTestAttr in test/testUtils.js
-> checkProps in test/testUtils.js
-> Did not abstract setup()
   . Too different for each components
-> Enzyme adapter in setupTest.js
-> Caution: too many abstractions = hard-to-read tests
   . Less useful in diagnosing failing tests


   ```````````````````````Redux Planning ````````````````````````````````
   `````````````````````````Component````````````````````````````````````
   -> App
      . Title, contains children components
   -> Input
      . Input box and submit Button
   -> GuessedWords
      . Table of guessed words
   -> Congrats
      . Congratulations message 

  ````````````````````````````Redux State`````````````````````````````````

  ..............................................................................................
  `...Key.....``.....DataType....``..Description...................``.....Starting value............ scretword      string          Word the user is trying to guess     word from server
  ... success        boolean         Whether or not the word has been     false
                                     guessed correctly               
  ... guessedWords   array ofobjects Array of objects:                    []
                                     { 
                                       guessedWord: string,
                                       letterMatchCount: number    
                                     }
  ........................... Simple Redux .....................................................
  -> Work with success piece of state
  -> Action creator creates CORRECT_GUESS action
  -> Reducer updates success
  -> Input conditionally renders
  .......................... Redux Thunk .......................................................
  -> Action creators that fire off multiple actions
  -> guessword
     . Add to guessedWords
     . Conditionally update success

  .......................... Redux Props in Connected Components ................................-> Test state and action creator props in
     . Input
     . App
  -> Test action creator calls

  ...............................................................................................
  -> npm install --save redux react-redux

  ...................... Success state planning .............................
  -> Action creator for CORRECT_GUESS action
  -> Success reducer
     . Controls success piece of state
     . Start with value of false
     . Flip success to true upon CORRECT_GUESS action
     


.............................................................................
.................................. What we did ..............................
1) Created a storeFactory utility
  -> Creates  a                                                                                                       testing store with app reducers
  -> will eventually add middlewares
2) Added it as a prop to our connected component
3) Used shallow to create a virtual DOM
4) Used .dive() to get child component (Input)

.............................................................................
.......................... New action creator: guessword ....................

1) Function will take a guessWord string
2) Use helper function to calculate letterMatchCount
3) Create action GUESS_WORD
   -> Payload contains guessedWord and letterMatchCount
4) Reducer will upload guessedWords state

.............................................................................
.......................... Two Complications ................................

1) How will it get secretWord to calculate letter match ?
2) What if the word was guessed successfully ?
   -> Right place to dispatch CORRECT_GUESS to update success state

............................................................................
.......................... Enter Redux thunk ................................

1) Two Actions from one creator? Need to access state ?

-> npm install redux-thunk

............................................................................
.......................... What can we do with store .......................
1) store.dispatch()
   -> Takes an action creator
2) store.getState()
   -> Returns state object
   -> Useful for assertions

............................................................................
.......................... Testing a Thunk .................................
1) Create a store with initial state
   -> Will contain 'secretWord'
2) Dispatch action creator
   -> store.dispatch(guessWord())
3) Check state
   -> Use Jests .toEqual() to test state object as whole


............................................................................
.......................... This is Integration Testing .....................

1) Testing action creators and reducer together
2) Where to put the tests?
   -> Make a new file: src/integration.test.js
   -> Would separate into many files for a larger app

............................................................................
.......................... Thunk Integration Testing .......................

1) Create a store with initial state
2) Dispatch action creator
3) Check state

............................................................................
.......................... Why Moxios ......................................
1) Random word server is necessay for actual app
2) Do not want to test server when testing app
3) Using moxios let us test app
   -> without testing server
   -> without even running server
............................................................................
.......................... How Moxios works ................................
1) Test installs moxios
   -> Axios will now send requests to moxios instead http
   -> test specifies moxios response

        Test -> Action -> Axios ----> Moxios
                Creator         ---->< http
2) Test calls action creator
3) Action creator calls axios
   -> Axios uses moxios instead of http for request
4) Action creator receives moxios response from axios

............................................................................
.......................... How Moxios works ................................

1) Test calls moxios.install()
   -> Sets moxios as the axios adapter
   -> Routes axios calls to moxios instead of http 
2) Can pass axios Instance to moxios.install()
   -> Use your configured settings

............................................................................
.......................... moxios.wait() ...................................
1) Call moxios.wait() during test
2) Watches for axios calls
3) Sends reponse using the callback passed to .wait()

............................................................................
...................... Testing Async Action Creator ........................
1) Create store using storeFactory()
2) Async actions: store.dispatch() returns promise
3) Put tests in .then() callback
   -> Tests will run after dispatch completes
   
